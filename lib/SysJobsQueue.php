<?php

namespace Application\Utils;

/**
 * This porgram is used to run *nix command line programs. This
 * creates and queue process for jobs and run them in organized way.
 *
 * @author Muneer Saheed
 */
class SysJobsQueue
{

    private $currentJobs;
    private $jobsQueue;
    private $maxCount;

    public function __construct(array $jobs, $concurrent = 3)
    {
        $this->currentJobs = array();
        $this->jobsQueue = $jobs;
        $this->maxCount = $concurrent;
    }

    public static function RunJobs(array $jobs, $concurrent)
    {
        $jobsQueue = new SysJobsQueue($jobs, $concurrent);
        return $jobsQueue->run();
    }

    private function run()
    {
        if (empty($this->jobsQueue))
            return false;

        return $this->processQueue();
    }

    private function processQueue()
    {
        while ($this->hasMoreJobs() || $this->hasRunningJobs())
        {
            $this->checkRunningPIDs();

            if ($this->hasMoreJobs())
            {
                if (!$this->reachedMaxJobs())
                {                    
                    // not reached max
                    $job = array_pop($this->jobsQueue);
                    $pid = $this->processOneMore($job);
                    if ($pid)
                    {
                        array_push($this->currentJobs, array('pid' => $pid, 'job' => $job));
                    }
                    continue;
                }
                else
                {
                    // reached max
                    sleep(3);
                    continue;
                }
            }
            sleep(2);
        }
        return true;
    }

    private function processOneMore($job)
    {
        $command = $this->prepareCommand($job);
        $pid = exec($command);
        return $pid;
    }

    private function hasRunningJobs()
    {
        if (count($this->currentJobs) > 0)
            return true;
        else
            return false;
    }

    private function prepareCommand($command)
    {
        $command = 'nohup ' . $command . ' > /dev/null 2>&1 & echo $!';
        return $command;
    }

    private function checkRunningPIDs()
    {
        // check if any of running process are completed and remove from runing jobs
        if (count($this->currentJobs) == 0)
            return;

        foreach ($this->currentJobs as $key => $PidWithJob)
        {
            $pid = $PidWithJob['pid'];
            exec('ps ' . $pid, $processState);
            if (count($processState) < 2)
            {
                // remove this item from current jobs
                unset($this->currentJobs[$key]);
            }
        }
    }

    private function hasMoreJobs()
    {
        $waitingCount = count($this->jobsQueue);
        if ($waitingCount > 0)
            return true;
        else
            return false;
    }

    private function reachedMaxJobs()
    {
        if (count($this->currentJobs) < $this->maxCount)
            return false;
        else
            return true;
    }

}
